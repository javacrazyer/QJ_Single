//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace QJY.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BI_DB_YBP
    {
        public int ID { get; set; }
        public Nullable<int> DimID { get; set; }
        public string Name { get; set; }
        public string YBContent { get; set; }
        public string YBType { get; set; }
        public string YBOption { get; set; }
        public string CRUser { get; set; }
        public Nullable<System.DateTime> CRDate { get; set; }
        public string Remark { get; set; }
    }
}
